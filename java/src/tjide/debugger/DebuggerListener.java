/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package tjide.debugger;

/**
 * DebuggerListener is a callback interface to monitor the progress of a
 * runTarget().
 */
public interface DebuggerListener {

    /**
     * Notify of debugged program exit.
     *
     * @param exitCode the return code of the debugged program
     */
    public void setDebugProgramExited(final int exitCode);

    /**
     * Save the Scope at a specific execution location.
     *
     * @param debugScope the scope to save
     */
    public void setDebugScope(final Scope debugScope);

    /**
     * Get the last seen debug scope.
     *
     * @return the debug scope
     */
    public Scope getDebugScope();

    /**
     * Set the debugger callback interface, so that the DebuggerListener can
     * control the program.
     *
     * @param debugger the debugger
     */
    public void setDebugger(final Debugger debugger);

}
