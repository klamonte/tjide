/*
 * Tranquil Java Integrated Development Environment
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package tjide.ui;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import gjexer.TTerminalWindow;

import tjide.project.FileTarget;

/**
 * ExternalEditorWindow is a terminal shell wrapper to an editor.
 */
public class ExternalEditorWindow extends TTerminalWindow
                                  implements TargetEditor {

    /**
     * Translated strings.
     */
    private static final ResourceBundle i18n = ResourceBundle.getBundle(ExternalEditorWindow.class.getName());

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------


    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The target associated with this file editor.
     */
    private FileTarget target;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param parent the main application
     */
    public ExternalEditorWindow(final TranquilApplication parent) {

        super(parent, 0, 0, getCommandLineNew(parent));

        setTitle(i18n.getString("editorTitle"));
        newStatusBar(i18n.getString("statusBarRunning"));
    }

    /**
     * Public constructor.
     *
     * @param parent the main application
     * @param filename a file to open
     */
    public ExternalEditorWindow(final TranquilApplication parent,
        final String filename) {

        super(parent, 0, 0, getCommandLineOpen(parent, filename));

        setTitle(MessageFormat.format(i18n.getString("editorTitleFilename"),
                filename));
        newStatusBar(i18n.getString("statusBarRunning"));
    }

    /**
     * Public constructor.
     *
     * @param parent the main application
     * @param filename a file to open
     * @param line a line number to open to
     */
    public ExternalEditorWindow(final TranquilApplication parent,
        final String filename, final int line) {

        super(parent, 0, 0, getCommandLineOpen(parent, filename, line));

        setTitle(MessageFormat.format(i18n.getString("editorTitleFilename"),
                filename));
        newStatusBar(i18n.getString("statusBarRunning"));
    }

    // ------------------------------------------------------------------------
    // Event handlers ---------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Handle window close.
     */
    @Override
    public void onClose() {
        ((TranquilApplication) getApplication()).removeEditor(target);
        super.onClose();
    }

    // ------------------------------------------------------------------------
    // TTerminalWindow --------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Hook for subclasses to be notified of the shell termination.
     */
    @Override
    public void onShellExit() {
        close();
    }

    // ------------------------------------------------------------------------
    // TargetEditor -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Get the Target associated with this editor.
     *
     * @return the target
     */
    public FileTarget getTarget() {
        return target;
    }

    /**
     * Set the Target associated with this editor.  Note package private
     * access.
     *
     * @param target the target
     */
    public void setTarget(final FileTarget target) {
        this.target = target;
    }

    // ------------------------------------------------------------------------
    // ExternalEditorWindow ---------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Replace $VISUAL and $EDITOR with environment options.
     *
     * @param command the original command line
     * @return the command line with $VISUAL and $EDITOR replaced
     */
    private static String replaceEnvVars(final String command) {
        String result = command;
        String visualEnv = System.getenv("VISUAL");
        String editorEnv = System.getenv("EDITOR");
        if (visualEnv != null) {
            result = result.replace("$VISUAL", visualEnv);
        }
        if (editorEnv != null) {
            result = result.replace("$EDITOR", editorEnv);
        }
        return result;
    }

    /**
     * Get the appropriate command line for the editor.
     *
     * @param app the main application
     * @return the command to spawn an editor
     */
    private static String getCommandLineNew(final TranquilApplication app) {
        String editorBin = app.getOption("editor.external.new");
        return replaceEnvVars(editorBin);
    }

    /**
     * Get the appropriate command line for the editor.
     *
     * @param app the main application
     * @param filename a file to open
     * @return the command to spawn an editor
     */
    private static String getCommandLineOpen(final TranquilApplication app,
        final String filename) {

        String editorBin = app.getOption("editor.external.open");
        String command = MessageFormat.format(editorBin, filename);
        return replaceEnvVars(command);
    }

    /**
     * Get the appropriate command line for the editor.
     *
     * @param app the main application
     * @param filename a file to open
     * @param line a line number to open to
     * @return the command to spawn an editor
     */
    private static String getCommandLineOpen(final TranquilApplication app,
        final String filename, final int line) {

        String editorBin = app.getOption("editor.external.openToLine");
        String command = MessageFormat.format(editorBin, filename, line);
        return replaceEnvVars(command);
    }

}
