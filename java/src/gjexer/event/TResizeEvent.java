/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package gjexer.event;

/**
 * This class encapsulates a screen or window resize event.
 */
public class TResizeEvent extends TInputEvent {

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Resize events can be generated for either a total screen resize or a
     * widget/window resize.
     */
    public enum Type {
        /**
         * The entire screen size changed.
         */
        SCREEN,

        /**
         * A widget was resized.
         */
        WIDGET
    }

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The type of resize.
     */
    private Type type;

    /**
     * New width.
     */
    private int width;

    /**
     * New height.
     */
    private int height;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public contructor.
     *
     * @param type the Type of resize, Screen or Widget
     * @param width the new width
     * @param height the new height
     */
    public TResizeEvent(final Type type, final int width, final int height) {
        this.type   = type;
        this.width  = width;
        this.height = height;
    }

    // ------------------------------------------------------------------------
    // TResizeEvent -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Get resize type.
     *
     * @return SCREEN or WIDGET
     */
    public Type getType() {
        return type;
    }

    /**
     * Get the new width.
     *
     * @return width
     */
    public int getWidth() {
        return width;
    }

    /**
     * Get the new height.
     *
     * @return height
     */
    public int getHeight() {
        return height;
    }

    /**
     * Make human-readable description of this TResizeEvent.
     *
     * @return displayable String
     */
    @Override
    public String toString() {
        return String.format("Resize: %s width = %d height = %d",
            type, width, height);
    }

}
