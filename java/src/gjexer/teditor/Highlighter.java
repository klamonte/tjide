/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package gjexer.teditor;

import java.util.SortedMap;
import java.util.TreeMap;

import gjexer.bits.CellAttributes;
import gjexer.bits.Color;

/**
 * Highlighter provides color choices for certain text strings.
 */
public class Highlighter {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The highlighter colors.
     */
    private SortedMap<String, CellAttributes> colors;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor sets the theme to the default.
     */
    public Highlighter() {
        // NOP
    }

    // ------------------------------------------------------------------------
    // Highlighter ------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Set keyword highlighting.
     *
     * @param enabled if true, enable keyword highlighting
     */
    public void setEnabled(final boolean enabled) {
        if (enabled) {
            setJavaColors();
        } else {
            colors = null;
        }
    }

    /**
     * Set my field values to that's field.
     *
     * @param rhs an instance of Highlighter
     */
    public void setTo(final Highlighter rhs) {
        colors = new TreeMap<String, CellAttributes>();
        colors.putAll(rhs.colors);
    }

    /**
     * See if this is a character that should split a word.
     *
     * @param ch the character
     * @return true if the word should be split
     */
    public boolean shouldSplit(final int ch) {
        // For now, split on punctuation
        String punctuation = "'\"\\<>{}[]!@#$%^&*();:.,-+/*?";
        if (ch < 0x100) {
            if (punctuation.indexOf((char) ch) != -1) {
                return true;
            }
        }
        return false;
    }

    /**
     * Retrieve the CellAttributes for a named theme color.
     *
     * @param name theme color name, e.g. "twindow.border"
     * @return color associated with name, e.g. bold yellow on blue
     */
    public CellAttributes getColor(final String name) {
        if (colors == null) {
            return null;
        }
        CellAttributes attr = colors.get(name);
        return attr;
    }

    /**
     * Sets to defaults that resemble the Borland IDE colors.
     */
    public void setJavaColors() {
        colors = new TreeMap<String, CellAttributes>();

        CellAttributes color;

        String [] types = {
            "boolean", "byte", "short", "int", "long", "char", "float",
            "double", "void",
        };
        color = new CellAttributes();
        color.setForeColor(Color.GREEN);
        color.setBackColor(Color.BLUE);
        color.setBold(true);
        for (String str: types) {
            colors.put(str, color);
        }

        String [] modifiers = {
            "abstract", "final", "native", "private", "protected", "public",
            "static", "strictfp", "synchronized", "transient", "volatile",
        };
        color = new CellAttributes();
        color.setForeColor(Color.WHITE);
        color.setBackColor(Color.BLUE);
        color.setBold(true);
        for (String str: modifiers) {
            colors.put(str, color);
        }

        String [] keywords = {
            "new", "class", "interface", "extends", "implements",
            "if", "else", "do", "while", "for", "break", "continue",
            "switch", "case", "default",
        };
        color = new CellAttributes();
        color.setForeColor(Color.YELLOW);
        color.setBackColor(Color.BLUE);
        color.setBold(true);
        for (String str: keywords) {
            colors.put(str, color);
        }

        String [] operators = {
            "[", "]", "(", ")", "{", "}",
            "*", "-", "+", "/", "=", "%",
            "^", "&", "!", "<<", ">>", "<<<", ">>>",
            "&&", "||",
            ">", "<", ">=", "<=", "!=", "==",
            ",", ";", ".", "?", ":",
        };
        color = new CellAttributes();
        color.setForeColor(Color.CYAN);
        color.setBackColor(Color.BLUE);
        color.setBold(true);
        for (String str: operators) {
            colors.put(str, color);
        }

        String [] packageKeywords = {
            "package", "import",
        };
        color = new CellAttributes();
        color.setForeColor(Color.GREEN);
        color.setBackColor(Color.BLUE);
        color.setBold(true);
        for (String str: packageKeywords) {
            colors.put(str, color);
        }

    }

}
