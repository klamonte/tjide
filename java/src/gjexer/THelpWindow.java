/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package gjexer;

import java.util.ResourceBundle;

import gjexer.bits.CellAttributes;
import gjexer.event.TResizeEvent;
import gjexer.help.THelpText;
import gjexer.help.Topic;

/**
 * THelpWindow
 */
public class THelpWindow extends TWindow {

    /**
     * Translated strings.
     */
    private static final ResourceBundle i18n = ResourceBundle.getBundle(THelpWindow.class.getName());

    // ------------------------------------------------------------------------
    // Constants --------------------------------------------------------------
    // ------------------------------------------------------------------------

    // Default help topic keys.  Note package private access.
    static String HELP_HELP                     = "Help On Help";

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The help text window.
     */
    private THelpText helpText;

    /**
     * The "Contents" button.
     */
    private TButton contentsButton;

    /**
     * The "Index" button.
     */
    private TButton indexButton;

    /**
     * The "Previous" button.
     */
    private TButton previousButton;

    /**
     * The "Close" button.
     */
    private TButton closeButton;

    /**
     * The X position for the buttons.
     */
    private int buttonOffset = 14;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param application TApplication that manages this window
     * @param topic the topic to start on
     */
    public THelpWindow(final TApplication application, final String topic) {
        this (application, application.helpFile.getTopic(topic));
    }

    /**
     * Public constructor.
     *
     * @param application TApplication that manages this window
     * @param topic the topic to start on
     */
    public THelpWindow(final TApplication application, final Topic topic) {
        super(application, i18n.getString("windowTitle"),
            1, 1, 78, 22, CENTERED | RESIZABLE);

        setMinimumWindowHeight(16);
        setMinimumWindowWidth(30);

        helpText = new THelpText(this, topic, 1, 1,
            getWidth() - buttonOffset - 4, getHeight() - 4);

        setHelpTopic(topic);

        // Buttons
        previousButton = addButton(i18n.getString("previousButton"),
            getWidth() - buttonOffset, 4,
            new TAction() {
                public void DO() {
                    if (application.helpTopics.size() > 1) {
                        Topic previous = application.helpTopics.remove(
                            application.helpTopics.size() - 2);
                        application.helpTopics.remove(application.
                            helpTopics.size() - 1);
                        setHelpTopic(previous);
                    }
                }
            });

        contentsButton = addButton(i18n.getString("contentsButton"),
            getWidth() - buttonOffset, 6,
            new TAction() {
                public void DO() {
                    setHelpTopic(application.helpFile.getTableOfContents());
                }
            });

        indexButton = addButton(i18n.getString("indexButton"),
            getWidth() - buttonOffset, 8,
            new TAction() {
                public void DO() {
                    setHelpTopic(application.helpFile.getIndex());
                }
            });

        closeButton = addButton(i18n.getString("closeButton"),
            getWidth() - buttonOffset, 10,
            new TAction() {
                public void DO() {
                    // Don't copy anything, just close the window.
                    THelpWindow.this.close();
                }
            });

        // Save this for last: make the close button default action.
        activate(closeButton);

    }

    /**
     * Public constructor.
     *
     * @param application TApplication that manages this window
     */
    public THelpWindow(final TApplication application) {
        this(application, HELP_HELP);
    }

    // ------------------------------------------------------------------------
    // Event handlers ---------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Handle window/screen resize events.
     *
     * @param event resize event
     */
    @Override
    public void onResize(final TResizeEvent event) {
        if (event.getType() == TResizeEvent.Type.WIDGET) {

            previousButton.setX(getWidth() - buttonOffset);
            contentsButton.setX(getWidth() - buttonOffset);
            indexButton.setX(getWidth() - buttonOffset);
            closeButton.setX(getWidth() - buttonOffset);

            helpText.setDimensions(1, 1, getWidth() - buttonOffset - 4,
                getHeight() - 4);
            helpText.onResize(new TResizeEvent(TResizeEvent.Type.WIDGET,
                    helpText.getWidth(), helpText.getHeight()));

            return;
        } else {
            super.onResize(event);
        }
    }

    // ------------------------------------------------------------------------
    // TWindow ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Retrieve the background color.
     *
     * @return the background color
     */
    @Override
    public final CellAttributes getBackground() {
        return getTheme().getColor("thelpwindow.background");
    }

    /**
     * Retrieve the border color.
     *
     * @return the border color
     */
    @Override
    public CellAttributes getBorder() {
        if (inWindowMove) {
            return getTheme().getColor("thelpwindow.windowmove");
        }
        return getTheme().getColor("thelpwindow.background");
    }

    /**
     * Retrieve the color used by the window movement/sizing controls.
     *
     * @return the color used by the zoom box, resize bar, and close box
     */
    @Override
    public CellAttributes getBorderControls() {
        return getTheme().getColor("thelpwindow.border");
    }

    // ------------------------------------------------------------------------
    // THelpWindow ------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Set the topic to display.
     *
     * @param topic the topic to display
     */
    public void setHelpTopic(final String topic) {
        setHelpTopic(getApplication().helpFile.getTopic(topic));
    }

    /**
     * Set the topic to display.
     *
     * @param topic the topic to display
     */
    private void setHelpTopic(final Topic topic) {
        boolean separator = true;
        if ((topic == getApplication().helpFile.getTableOfContents())
            || (topic == getApplication().helpFile.getIndex())
        ) {
            separator = false;
        }

        getApplication().helpTopics.add(topic);
        helpText.setTopic(topic, separator);
    }

}
