/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package gjexer;

import gjexer.bits.CellAttributes;
import gjexer.bits.MnemonicString;
import gjexer.bits.StringUtils;

/**
 * TLabel implements a simple label, with an optional mnemonic hotkey action
 * associated with it.
 */
public class TLabel extends TWidget {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The shortcut and label.
     */
    private MnemonicString mnemonic;

    /**
     * The action to perform when the mnemonic shortcut is pressed.
     */
    private TAction action;

    /**
     * Label color.
     */
    private String colorKey;

    /**
     * If true, use the window's background color.
     */
    private boolean useWindowBackground = true;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor, using the default "tlabel" for colorKey.
     *
     * @param parent parent widget
     * @param text label on the screen
     * @param x column relative to parent
     * @param y row relative to parent
     */
    public TLabel(final TWidget parent, final String text, final int x,
        final int y) {

        this(parent, text, x, y, "tlabel");
    }

    /**
     * Public constructor, using the default "tlabel" for colorKey.
     *
     * @param parent parent widget
     * @param text label on the screen
     * @param x column relative to parent
     * @param y row relative to parent
     * @param action to call when shortcut is pressed
     */
    public TLabel(final TWidget parent, final String text, final int x,
        final int y, final TAction action) {

        this(parent, text, x, y, "tlabel", action);
    }

    /**
     * Public constructor.
     *
     * @param parent parent widget
     * @param text label on the screen
     * @param x column relative to parent
     * @param y row relative to parent
     * @param colorKey ColorTheme key color to use for foreground text
     */
    public TLabel(final TWidget parent, final String text, final int x,
        final int y, final String colorKey) {

        this(parent, text, x, y, colorKey, true);
    }

    /**
     * Public constructor.
     *
     * @param parent parent widget
     * @param text label on the screen
     * @param x column relative to parent
     * @param y row relative to parent
     * @param colorKey ColorTheme key color to use for foreground text
     * @param action to call when shortcut is pressed
     */
    public TLabel(final TWidget parent, final String text, final int x,
        final int y, final String colorKey, final TAction action) {

        this(parent, text, x, y, colorKey, true, action);
    }

    /**
     * Public constructor.
     *
     * @param parent parent widget
     * @param text label on the screen
     * @param x column relative to parent
     * @param y row relative to parent
     * @param colorKey ColorTheme key color to use for foreground text
     * @param useWindowBackground if true, use the window's background color
     */
    public TLabel(final TWidget parent, final String text, final int x,
        final int y, final String colorKey, final boolean useWindowBackground) {

        this(parent, text, x, y, colorKey, useWindowBackground, null);
    }

    /**
     * Public constructor.
     *
     * @param parent parent widget
     * @param text label on the screen
     * @param x column relative to parent
     * @param y row relative to parent
     * @param colorKey ColorTheme key color to use for foreground text
     * @param useWindowBackground if true, use the window's background color
     * @param action to call when shortcut is pressed
     */
    public TLabel(final TWidget parent, final String text, final int x,
        final int y, final String colorKey, final boolean useWindowBackground,
        final TAction action) {

        // Set parent and window
        super(parent, false, x, y, 0, 1);

        setLabel(text);
        this.colorKey = colorKey;
        this.useWindowBackground = useWindowBackground;
        this.action = action;
    }

    // ------------------------------------------------------------------------
    // TWidget ----------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Override TWidget's width: we can only set width at construction time.
     *
     * @param width new widget width (ignored)
     */
    @Override
    public void setWidth(final int width) {
        // Do nothing
    }

    /**
     * Override TWidget's height: we can only set height at construction
     * time.
     *
     * @param height new widget height (ignored)
     */
    @Override
    public void setHeight(final int height) {
        // Do nothing
    }

    /**
     * Draw a static label.
     */
    @Override
    public void draw() {
        // Setup my color
        CellAttributes color = new CellAttributes();
        CellAttributes mnemonicColor = new CellAttributes();
        color.setTo(getTheme().getColor(colorKey));
        mnemonicColor.setTo(getTheme().getColor("tlabel.mnemonic"));
        if (useWindowBackground) {
            CellAttributes background = getWindow().getBackground();
            color.setBackColor(background.getBackColor());
            mnemonicColor.setBackColor(background.getBackColor());
        }
        putStringXY(0, 0, mnemonic.getRawLabel(), color);
        if (mnemonic.getScreenShortcutIdx() >= 0) {
            putCharXY(mnemonic.getScreenShortcutIdx(), 0,
                mnemonic.getShortcut(), mnemonicColor);
        }
    }

    // ------------------------------------------------------------------------
    // TLabel -----------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Get label raw text.
     *
     * @return label text
     */
    public String getLabel() {
        return mnemonic.getRawLabel();
    }

    /**
     * Get the mnemonic string for this label.
     *
     * @return mnemonic string
     */
    public MnemonicString getMnemonic() {
        return mnemonic;
    }

    /**
     * Set label text.
     *
     * @param label new label text
     */
    public void setLabel(final String label) {
        mnemonic = new MnemonicString(label);
        super.setWidth(StringUtils.width(mnemonic.getRawLabel()));
    }

    /**
     * Get the label color.
     *
     * @return the ColorTheme key color to use for foreground text
     */
    public String getColorKey() {
        return colorKey;
    }

    /**
     * Set the label color.
     *
     * @param colorKey ColorTheme key color to use for foreground text
     */
    public void setColorKey(final String colorKey) {
        this.colorKey = colorKey;
    }

    /**
     * Act as though the mnemonic shortcut was pressed.
     */
    public void dispatch() {
        if (action != null) {
            action.DO(this);
        }
    }

}
