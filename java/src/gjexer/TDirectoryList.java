/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package gjexer;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gjexer.bits.StringUtils;

/**
 * TDirectoryList shows the files within a directory.
 */
public class TDirectoryList extends TList {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Files in the directory.
     */
    private Map<String, File> files;

    /**
     * Root path containing files to display.
     */
    private File path;

    /**
     * The list of filters that a file must match in order to be displayed.
     */
    private List<String> filters;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.
     *
     * @param parent parent widget
     * @param path directory path, must be a directory
     * @param x column relative to parent
     * @param y row relative to parent
     * @param width width of text area
     * @param height height of text area
     */
    public TDirectoryList(final TWidget parent, final String path, final int x,
        final int y, final int width, final int height) {

        this(parent, path, x, y, width, height, null, null, null);
    }

    /**
     * Public constructor.
     *
     * @param parent parent widget
     * @param path directory path, must be a directory
     * @param x column relative to parent
     * @param y row relative to parent
     * @param width width of text area
     * @param height height of text area
     * @param action action to perform when an item is selected (enter or
     * double-click)
     */
    public TDirectoryList(final TWidget parent, final String path, final int x,
        final int y, final int width, final int height, final TAction action) {

        this(parent, path, x, y, width, height, action, null, null);
    }

    /**
     * Public constructor.
     *
     * @param parent parent widget
     * @param path directory path, must be a directory
     * @param x column relative to parent
     * @param y row relative to parent
     * @param width width of text area
     * @param height height of text area
     * @param action action to perform when an item is selected (enter or
     * double-click)
     * @param singleClickAction action to perform when an item is selected
     * (single-click)
     */
    public TDirectoryList(final TWidget parent, final String path, final int x,
        final int y, final int width, final int height, final TAction action,
        final TAction singleClickAction) {

        this(parent, path, x, y, width, height, action, singleClickAction,
            null);
    }

    /**
     * Public constructor.
     *
     * @param parent parent widget
     * @param path directory path, must be a directory
     * @param x column relative to parent
     * @param y row relative to parent
     * @param width width of text area
     * @param height height of text area
     * @param action action to perform when an item is selected (enter or
     * double-click)
     * @param singleClickAction action to perform when an item is selected
     * (single-click)
     * @param filters a list of strings that files must match to be displayed
     */
    public TDirectoryList(final TWidget parent, final String path, final int x,
        final int y, final int width, final int height, final TAction action,
        final TAction singleClickAction, final List<String> filters) {

        super(parent, null, x, y, width, height, action);
        files = new HashMap<String, File>();
        this.filters = filters;
        this.singleClickAction = singleClickAction;

        setPath(path);
    }

    // ------------------------------------------------------------------------
    // TList ------------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // TDirectoryList ---------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Set the new path to display.
     *
     * @param path new path to list files for
     */
    public void setPath(final String path) {
        this.path = new File(path);

        List<String> newStrings = new ArrayList<String>();
        files.clear();

        // Build a list of files in this directory
        File [] newFiles = this.path.listFiles();
        if (newFiles != null) {
            for (int i = 0; i < newFiles.length; i++) {
                if (newFiles[i].getName().startsWith(".")) {
                    continue;
                }
                if (newFiles[i].isDirectory()) {
                    continue;
                }
                if (filters != null) {
                    for (String pattern: filters) {

                        /*
                        System.err.println("newFiles[i] " +
                            newFiles[i].getName() + " " + pattern +
                            " " + newFiles[i].getName().matches(pattern));
                        */

                        if (newFiles[i].getName().matches(pattern)) {
                            String key = renderFile(newFiles[i]);
                            files.put(key, newFiles[i]);
                            newStrings.add(key);
                            break;
                        }
                    }
                } else {
                    String key = renderFile(newFiles[i]);
                    files.put(key, newFiles[i]);
                    newStrings.add(key);
                }
            }
        }
        setList(newStrings);

        // Select the first entry
        if (getMaxSelectedIndex() >= 0) {
            setSelectedIndex(0);
        }
    }

    /**
     * Get the path that is being displayed.
     *
     * @return the path
     */
    public File getPath() {
        path = files.get(getSelected());
        return path;
    }

    /**
     * Format one of the entries for drawing on the screen.
     *
     * @param file the File
     * @return the line to draw
     */
    private String renderFile(final File file) {
        String name = file.getName();
        if (StringUtils.width(name) > 20) {
            name = name.substring(0, 17) + "...";
        }
        return String.format("%-20s %5dk", name, (file.length() / 1024));
    }

}
