/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package gjexer;

/**
 * TInputBox is a system-modal dialog with an OK button and a text input
 * field.  Call it like:
 *
 * <pre>
 * {@code
 *     box = inputBox(title, caption);
 *     if (box.getText().equals("yes")) {
 *         ... the user entered "yes", do stuff ...
 *     }
 * }
 * </pre>
 *
 */
public class TInputBox extends TMessageBox {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * The input field.
     */
    private TField field;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Public constructor.  The input box will be centered on screen.
     *
     * @param application TApplication that manages this window
     * @param title window title, will be centered along the top border
     * @param caption message to display.  Use embedded newlines to get a
     * multi-line box.
     */
    public TInputBox(final TApplication application, final String title,
        final String caption) {

        this(application, title, caption, "", Type.OK);
    }

    /**
     * Public constructor.  The input box will be centered on screen.
     *
     * @param application TApplication that manages this window
     * @param title window title, will be centered along the top border
     * @param caption message to display.  Use embedded newlines to get a
     * multi-line box.
     * @param text initial text to seed the field with
     */
    public TInputBox(final TApplication application, final String title,
        final String caption, final String text) {

        this(application, title, caption, text, Type.OK);
    }

    /**
     * Public constructor.  The input box will be centered on screen.
     *
     * @param application TApplication that manages this window
     * @param title window title, will be centered along the top border
     * @param caption message to display.  Use embedded newlines to get a
     * multi-line box.
     * @param text initial text to seed the field with
     * @param type one of the Type constants.  Default is Type.OK.
     */
    public TInputBox(final TApplication application, final String title,
        final String caption, final String text, final Type type) {

        super(application, title, caption, type, false);

        for (TWidget widget: getChildren()) {
            if (widget instanceof TButton) {
                widget.setY(widget.getY() + 2);
            }
        }

        setHeight(getHeight() + 2);
        field = addField(1, getHeight() - 6, getWidth() - 4, false, text);

        // Set the secondaryThread to run me
        getApplication().enableSecondaryEventReceiver(this);

        // Yield to the secondary thread.  When I come back from the
        // constructor response will already be set.
        getApplication().yield();
    }

    // ------------------------------------------------------------------------
    // TMessageBox ------------------------------------------------------------
    // ------------------------------------------------------------------------

    // ------------------------------------------------------------------------
    // TInputBox --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Retrieve the answer text.
     *
     * @return the answer text
     */
    public String getText() {
        return field.getText();
    }

}
