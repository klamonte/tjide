/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package gjexer;

/**
 * A TAction represents a simple action to perform in response to the user.
 *
 * @see TButton
 */
public abstract class TAction {

    /**
     * The widget that called this action's DO() method.  Note that this
     * field could be null, for example if executed as a timer action.
     */
    public TWidget source;

    /**
     * An optional bit of data associated with this action.
     */
    public Object data;

    /**
     * Call DO() with source widget set.
     *
     * @param source the source widget
     */
    public final void DO(final TWidget source) {
        this.source = source;
        DO();
    }

    /**
     * Call DO() with source widget and data set.
     *
     * @param source the source widget
     * @param data the data
     */
    public final void DO(final TWidget source, final Object data) {
        this.source = source;
        this.data = data;
        DO();
    }

    /**
     * Various classes will call DO() when they are clicked/selected.
     */
    public abstract void DO();
}
