/*
 * GJexer - GPL Java Text User Interface
 *
 * The GNU General Public License Version 3
 *
 * Copyright (C) 2021 Autumn Lamonte
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Autumn Lamonte [AutumnWalksTheLake@gmail.com] ⚧ Trans Liberation Now
 * @version 1
 */
package gjexer;

import java.util.Date;

/**
 * TTimer implements a simple timer.
 */
public class TTimer {

    // ------------------------------------------------------------------------
    // Variables --------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * If true, re-schedule after every tick.  Note package private access.
     */
    boolean recurring = false;

    /**
     * Duration (in millis) between ticks if this is a recurring timer.
     */
    private long duration = 0;

    /**
     * The next time this timer needs to be ticked.
     */
    private Date nextTick;

    /**
     * The action to perfom on a tick.
     */
    private TAction action;

    // ------------------------------------------------------------------------
    // Constructors -----------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Package private constructor.
     *
     * @param duration number of milliseconds to wait between ticks
     * @param recurring if true, re-schedule this timer after every tick
     * @param action to perform on next tick
     */
    TTimer(final long duration, final boolean recurring, final TAction action) {

        this.recurring = recurring;
        this.duration  = duration;
        this.action    = action;

        Date now = new Date();
        nextTick = new Date(now.getTime() + duration);
    }

    // ------------------------------------------------------------------------
    // TTimer -----------------------------------------------------------------
    // ------------------------------------------------------------------------

    /**
     * Get the next time this timer needs to be ticked.  Note package private
     * access.
     *
     * @return time at which action should be called
     */
    Date getNextTick() {
        return nextTick;
    }

    /**
     * Set the recurring flag.
     *
     * @param recurring if true, re-schedule this timer after every tick
     */
    public void setRecurring(final boolean recurring) {
        this.recurring = recurring;
    }

    /**
     * Tick this timer.  Note package private access.
     */
    void tick() {
        if (action != null) {
            action.DO();
        }
        // Set next tick
        Date ticked = new Date();
        if (recurring) {
            nextTick = new Date(ticked.getTime() + duration);
        }
    }

}
