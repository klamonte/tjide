#!/bin/sh

# Tranquil Java Integrated Development Environment
# ================================================

# This script tries to find a working JVM and tjide.jar, and then run
# the selected UI.
#
# -----------------------------------------------------------------------------
# The GNU General Public License Version 3
#
# Copyright (C) 2021 Autumn Lamonte
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

# NOTE: if you want a different set of properties, simply prefix a
# directory containing tjide.properties to the classpath.

# -----------------------------------------------------------------------------
# Functions -------------------------------------------------------------------
# -----------------------------------------------------------------------------

display_help ()
{
    echo "Tranquil Java IDE"
    echo
    echo "Usage: tj [OPTIONS]"
    echo
    echo "Available options:"
    echo
    echo "  --project FILENAME   Open project FILENAME."
    echo "  --build              Issue Compile | Build command"
    echo "                       Requires --project be specied."
    echo "  --make               Issue Compile | Make command"
    echo "                       Requires --project be specied."
    echo "  --run                Issue Run | Run command"
    echo "                       Requires --project be specied."
    echo
    echo "  --width COLUMNS      Size window to at least COLUMNS wide."
    echo "  --height ROWS        Size window to at least ROWS tall."
    echo
    echo "  -t                   Use existing text (xterm) terminal"
    echo "  -w                   Open in a new Swing window"
    echo
    echo "  --version            Display program version and license information and exit"
    echo "  -h, --help, -?       Print this usage and exit"
    exit 1;
}

# -----------------------------------------------------------------------------
# Main ------------------------------------------------------------------------
# -----------------------------------------------------------------------------

# Start with bin and shared directories, add $(dirname $0) too.
JAR_PATHS="bin /usr/share/java $(dirname $(which $0))"
JAR_PATHS="$JAR_PATHS java/build/jar"

for i in $JAR_PATHS ; do
    if [ -f $i/tjide.jar ]; then
        TJIDE_JAR=$i/tjide.jar
    fi
done
if [ -z $TJIDE_JAR ]; then
    echo "Error: tjide.jar not found.  Looked in: $JAR_PATHS"
    return 1
fi
JAVA=java
UI=tjide.Main
OPTS=
TJARGS=

case "$1" in
    --project)
        TJARGS="$TJARGS $1 $2"
        shift
        shift
        ;;
    --width)
        TJARGS="$TJARGS $1 $2"
        shift
        shift
        ;;
    --height)
        TJARGS="$TJARGS $1 $2"
        shift
        shift
        ;;
    --build)
        TJARGS="$TJARGS $1"
        shift
        ;;
    --make)
        TJARGS="$TJARGS $1"
        shift
        ;;
    --run)
        TJARGS="$TJARGS $1"
        shift
        ;;
    --version)
        TJARGS="$TJARGS $1"
        shift
        ;;
    -h)
        display_help
        ;;
    --help)
        display_help
        ;;
    -t)
        # ECMA48/Xterm UI.  This will not work on the Windows console.
        OPTS="-Dgjexer.Swing=false"
        shift
        ;;
    -w)
        # Swing UI, naive output.  This is faster to start, but has
        # more screen tearing.

        # OPTS="-Dgjexer.Swing=true -Dgjexer.Swing.tripleBuffer=false"

        # Swing UI, triple-buffered output.  This may take longer to
        # start, but has less screen tearing.
        OPTS="-Dgjexer.Swing=true -Dgjexer.Swing.tripleBuffer=true"
        shift
        ;;
    -?)
        display_help
        ;;
    *)
        ;;
esac

# For debugging, enable assertions
OPTS="$OPTS -ea"
CMDLINE="$JAVA -cp $TJIDE_JAR $OPTS $UI $TJARGS $*"

# echo "Executing: $CMDLINE"
$CMDLINE
